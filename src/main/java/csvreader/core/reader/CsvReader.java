package csvreader.core.reader;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import static csvreader.core.helpers.PropertiesConfig.*;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import csvreader.core.Startup;
import csvreader.core.helpers.CsvHelper;

public class CsvReader {
	
	private static Logger logger = Logger.getLogger(CsvReader.class);
	
	
	private CsvHelper helper;
	
	public void readData() throws IOException {
		helper = new CsvHelper();
		
		try {
			FileReader filereader = helper.openFile();
			CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader csvReader = new CSVReaderBuilder(filereader).withCSVParser(parser).build();

			List<String[]> data = csvReader.readAll();

			
			for (String[] row : data) {
				for (String cell : row) {
					logger.debug(cell + "\t");
				}
				logger.error("");
			}
		} catch (Exception e) {
//			logger.error("Arquivo não carregado");
		}

	}

}
