package csvreader.core.helpers;

import static csvreader.core.helpers.PropertiesConfig.getProperty;
import static csvreader.core.helpers.PropertiesConfig.hasProperty;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;

import csvreader.core.helpers.CsvHelper;

public class CsvHelper {

	private String filePath;

	static Logger logger = Logger.getLogger(CsvHelper.class);

	public CsvHelper() throws IOException {

		String propertyName = "env.reader.csv.file";
		logger.debug("Trying to read property " + propertyName);

		if (hasProperty(propertyName)) {
			filePath = getProperty(propertyName);
		} else {
			logger.error("Property [" + propertyName + "] was not setted.");
		}

	}

	public FileReader openFile() throws FileNotFoundException {
		FileReader filereader = null;
		
		if (filePath != null && !filePath.isEmpty()) {
			filereader = new FileReader(getProperty("C:\\qa\\workspace\\csvreader\\src\\main\\resources"));

		}
		return filereader;
	}
}
