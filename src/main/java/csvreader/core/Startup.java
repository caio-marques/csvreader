package csvreader.core;

import org.apache.log4j.Logger;

import csvreader.core.Startup;
import csvreader.core.reader.CsvReader;

public class Startup {
	
	private static CsvReader reader;
	
	private static Logger logger = Logger.getLogger(Startup.class);
	
	public static void main(String[] args) {
		try {
			reader =  new CsvReader();
			reader.readData();
		}
		catch(Exception e){
			logger.error("Generic error", e);
		}
	}
	

}
